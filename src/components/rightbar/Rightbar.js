import './rightbar.css'
import {Users} from '../../dummyData'
import Online from '../online/Online'
import { useContext, useEffect, useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { AuthContext } from '../../context/AuthContext';
import { Add, Remove } from '@material-ui/icons';

function Rightbar({profile}) {
    //public_folder
    const PF= process.env.REACT_APP_PUBLIC_FOLDER;
    const [friends, setFriends] = useState([])
    const {user, dispatch} = useContext(AuthContext)
    const [followed, setFollowed] = useState(user.following.includes(profile?.id));

    // useEffect(() => {
    //     setFollowed(!user.following.includes(profile?.id));
    // }, [user, profile])

    useEffect(() => {
        const getFriends = async () => {
            try {
                const friendList = await axios.get("/users/friends/"+ profile._id);
                setFriends(friendList.data)
            } catch (err) {
                console.log(err)
            }
        };
        getFriends()
    }, [profile])

    const handleClick = async () => {
        try {
            if (followed) {
                await axios.put("/users/"+profile._id+"/unfollow", {userId: user._id});
                dispatch({ type:"UNFOLLOW", payload: profile._id })
            }
            else {
                await axios.put("/users/"+profile._id+"/follow", {userId: user._id});      
                dispatch({ type:"FOLLOW", payload: profile._id })        
            }
        } catch (err) {
            console.log(err)
        }
        setFollowed(!followed);
    }


    const HomeRightBar = () => {
        return (
            <div className="rightbarWrapper">
                <div className="birthdayContainer">
                    <img src="assets/gift.png" className="birthdayImg" alt="" />
                    <span className="birthdayText"><b>Raje</b> and <b>3 others</b> have birthday today.</span>
                </div>
                <img src="assets/ad.jpg" className="rightbarAd" alt="" />
                <h4 className="rightbarTitle">Online Friends</h4>
                <ul className="rightbarFriendList">
                    {Users.map(u => (
                        <Online key={u.id} user={u} />
                    ))}
                </ul>
            </div>
        )
    };

    const ProfileRightBar = () => {
        return (
            <>
                { profile.username !== user.username && (
                    <button className="rightbarFollowButton" onClick={handleClick}>
                        {followed? "Unfollow" : "follow"}
                        {followed? <Remove/> : <Add/>}
                    </button>
                )}
                <h4 className="rightbarTitle">User Information</h4>
                <div className="rightbarInfo">
                    <div className="rightbarInfoItem">
                        <span className="rightbarinfoKey">City:</span>
                        <span className="rightbarinfoValue">{profile.city}</span>
                    </div>
                    <div className="rightbarInfoItem">
                        <span className="rightbarinfoKey">From:</span>
                        <span className="rightbarinfoValue">{profile.from}</span>
                    </div>
                    <div className="rightbarInfoItem">
                        <span className="rightbarinfoKey">Relation:</span>
                        <span className="rightbarinfoValue">{profile.relationship === 1 ? "Single" : profile.relationship ===2 ? "Married" : "-"}</span>
                    </div>
                </div>
                <h4 className="rightBarTitle">User Friends</h4>
                <div className="rightbarFollowings">
                    {friends.map(friend => (
                        <Link to = {"/profile/"+friend.username} style={{textDecoration:"none"}}>
                            <div className="rightbarFollowing">
                                <img src={friend.profilePic ? PF+friend.profilePic : PF+"/person/defaultUser.png"} alt="" className="rightbarFollowingImg" />
                                <span className="rightbarFollowingName">{friend.username}</span>
                            </div>
                        </Link>
                    ))}
                </div>

            </>
        )
    }
    
    
    return (
        <div className="rightbar">
            {profile ? <ProfileRightBar/> : <HomeRightBar />}
        </div>
    )
}

export default Rightbar

