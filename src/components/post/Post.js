import { MoreVert } from "@material-ui/icons"
import "./post.css"
import { useContext, useEffect, useState } from "react";
import axios from "axios";
import { format } from "timeago.js";
import { Link } from "react-router-dom";
import { AuthContext } from "../../context/AuthContext";

const Post = ({post}) => {
    const [like, setLike] = useState(post.likes.length)
    const [isLiked, setIsLiked] = useState(false)
    const [user, setUser] = useState({})
    const {user:currentUser} = useContext(AuthContext)
    
    //public_folder
    const PF= process.env.REACT_APP_PUBLIC_FOLDER;

    useEffect (()=> {
        setIsLiked(post.likes.includes(currentUser._id))
    }, [currentUser._id, post.likes])

    useEffect (() => {
        const getUser= async () => {
            const response = await axios.get(`/users?userId=`+post.userId);
            setUser(response.data)
        }
        getUser()
    }, [post.userId])


    const likeHandler = () => {
        try {
            axios.put("/posts/"+post._id+"/like", {userId: currentUser._id});
        }
        catch (err){
            console.log(err)
        }
        setLike(isLiked ? like-1 : like+1);
        setIsLiked(!isLiked)
    }

    return (
        <div className="post">
            <div className="postWrapper">
                <div className="postTop">
                    <div className="postTopLeft">
                        <Link to={`profile/${user.username}`}>
                            <img src={ user.profilePic ? PF + user.profilePic : PF+"person/defaultUser.png"} alt="" className="postProfileImg" />
                        </Link>
                        <span className="postUsername">{user.username}</span>
                        <span className="postDate">{format(post.createdAt)}</span>
                    </div>
                    <div className="postTopRight">
                        <MoreVert />
                    </div>
                </div>
                <div className="postCenter">
                    <span className="postText">
                        {post?.desc}
                    </span>
                    <img src={PF+post.img} alt="" className="postImg" />
                </div>
                <div className="postBottom">
                    <div className="postBottomLeft">
                        <img src={PF+"like.png"} alt="" onClick={likeHandler} className="likeIcon" />
                        <img src={PF+"heart.png"} alt="" onClick={likeHandler} className="likeIcon"/>
                        <span className="postLikeCounter">{like} people like it</span>
                    </div>
                    <div className="postBottomRight">
                        <div className="postCommentText">{post.comment} comments</div>
                    </div>
                </div>
                

            </div>
        </div>
    )
}

export default Post
