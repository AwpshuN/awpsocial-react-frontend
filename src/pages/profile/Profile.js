import "./profile.css"
import React, { useEffect, useState } from 'react'
import Feed from '../../components/feed/Feed'
import Sidebar from '../../components/sidebar/Sidebar'
import Topbar from '../../components/topbar/Topbar'
import Rightbar from '../../components/rightbar/Rightbar'
import axios from "axios"
import { useParams } from "react-router";

function Profile() {
    const [user, setUser] = useState({})
    const username = useParams().username; // .username coz we defined /profile/:username in app js {try console.log}
    const PF= process.env.REACT_APP_PUBLIC_FOLDER;

    useEffect (() => {
        const getUser= async () => {
            const response = await axios.get(`/users?username=${username}`);
            setUser(response.data)
        }
        getUser()
    }, [username])

    return (
        <>
            <Topbar />
            <div className="profile">
                <Sidebar />
                <div className="profileRight">
                    <div className="profileRightTop">
                        <div className="profileCover">
                            <img src={user.coverPic ? PF+user.coverPic : PF+"post/defaultCover.jpg"} alt="" className="profileCoverImg" />
                            <img src={ user.profilePic ? PF + user.profilePic : PF+"person/defaultUser.png"} alt="" className="profileUserImg" />
                        </div>
                        <div className="profileInfo">
                            <h4 className="profielInfoName">{user.username}</h4>
                            <p className="profileInfoDesc">{user.desc}</p>
                        </div>
                    </div>
                    <div className="profileRightBottom">
                        <Feed username={username}/>
                        <Rightbar profile={user}/>
                    </div>
                </div>
            </div>      
        </>
    )
}

export default Profile
