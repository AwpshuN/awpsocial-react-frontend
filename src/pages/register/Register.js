import "./register.css"
import { Link, useHistory } from "react-router-dom";
import { useRef } from "react";
import axios from "axios";

function Register() {
    const email = useRef();
    const username = useRef();
    const phone = useRef();
    const password = useRef();
    const confirmPassword = useRef();
    const history = useHistory();

    const handleClick = async (e) =>{
        e.preventDefault();
        if (confirmPassword.current.value !== password.current.value) {
            confirmPassword.current.setCustomValidity("Passwords don't match!");
        }
        else {
            const user ={
                username: username.current.value,
                email: email.current.value,
                password: password.current.value,
                phone: phone.current.value,
            }
            console.log(user)
            try{
                await axios.post("/auth/register", user);
                history.push("/login")
            } catch (err) {
                console.log(err)
            }
        }
    };

    return (
        <div className="login">
            <div className="loginWrapper">
                <div className="loginLeft">
                    <h3 className="loginLogo">Awpsocial</h3>
                    <span className="loginDesc">A social site developed by a beginning MERN student. Join PLOX!!!</span>
                </div>
                <div className="loginRight">
                    <form className="loginBox" onSubmit={handleClick}>
                        <input type="text" placeholder="Username" className="loginInput" required ref={username}/>
                        <div className="">
                            <input type="text" placeholder="First Name" className="loginInput" required/>
                            <input type="text" placeholder="Last Name" className="loginInput lastname" required/>
                        </div>
                        <input type="email" placeholder="Email" className="loginInput" ref={email}/>
                        <input type="text" placeholder="Phone Number" className="loginInput" required maxLength="10" ref={phone}/>
                        <input type="password" placeholder="Password" className="loginInput" ref={password} minLength="6" required/>
                        <input type="password" placeholder="Confirm Password" className="loginInput" ref={confirmPassword} required/>
                        <button className="loginButton" >Sign Up</button>
                        <button className="loginRegisterButton"><Link to="/login" style={{textDecoration:"none"}}>Log into Account</Link></button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default Register
