import "./login.css"
import { useContext, useRef } from "react";
import { loginCall } from "../../components/apiCalls";
import { AuthContext } from "../../context/AuthContext";
import { CircularProgress } from "@material-ui/core";

function Login() {

    const email = useRef()
    const password = useRef()
    const { user, isFetching, dispatch} = useContext(AuthContext)

    const handleClick = (e) =>{
        e.preventDefault();
        loginCall( { email: email.current.value, password: password.current.value },
             dispatch );
    };

    console.log(user)
    
    return (
        <div className="login">
            <div className="loginWrapper">
                <div className="loginLeft">
                    <h3 className="loginLogo">Awpsocial</h3>
                    <span className="loginDesc">A social site developed by a beginning MERN student. Join PLOX!!!</span>
                </div>
                <div className="loginRight">
                    <form className="loginBox" onSubmit={handleClick}>
                        <input type="email" placeholder="Email" className="loginInput" required ref={email} />
                        <input type="password" placeholder="Password" className="loginInput" minLength="6" required ref= {password}/>
                        <button className="loginButton" disabled={isFetching}>
                            { isFetching 
                                ? <CircularProgress color = "white" size="20px"/> 
                                : "Login"
                            }
                        </button>
                        <span className="loginForgot">Forgot Password?</span>
                        <button className="loginRegisterButton">
                            { isFetching 
                                ? <CircularProgress color = "white" size="20px"/> 
                                : "Create a new account"
                                }
                        </button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default Login
